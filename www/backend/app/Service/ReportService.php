<?php


namespace App\Service;


use App\Library\Traits\IceAndFireTrait;

class ReportService
{
    use IceAndFireTrait;


    /**
     * Filtering api response report with any of the available parameter
     * supported by the ice and fire endpoint.
     * @param $params
     * @param $query
     * @return array|string
     */
    public function getFilteredReports($params, $query)
    {
        return $this->getWithFilter("reports?$params=$query");
    }

}
