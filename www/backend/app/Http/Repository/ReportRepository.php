<?php

/************************************
 ** File: Report Repository file  ******
 ** Date: 10th June 2020  ************
 ** Report Repository file  ************
 ** Author: Asefon pelumi M. *********
 ** Senior Software Developer ********
 * Email: pelumiasefon@gmail.com  ***
 * **********************************/

namespace App\Http\Repository;


use App\Library\Providers\SearchProvider\Factories\SearchFactory;
use App\Library\Traits\IceAndFireTrait;
use App\Models\Report;
use App\Service\ReportService;
use function GuzzleHttp\Promise\all;

class ReportRepository
{

    private $report;
    private $reportService;

    /**
     * ReportRepository constructor.
     * @param Report $report
     * @param ReportService $reportService
     */
    public function __construct(Report $report, ReportService $reportService)
    {
        $this->report = $report;
        $this->reportService = $reportService;
    }


    /**
     * functions to get all reports
     *
     * @return Report[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAllReports()
    {
        return $this->report->all();
    }

//     get single report in the application

    /**
     * @param $table_field
     * @param $query
     * @return mixed
     */
    public function getSingleReport($table_field, $query)
    {
        return $this->report->where($table_field, $query)->first();
    }


    /**
     * function to update user details
     *
     * @param $request
     * @return Report
     */
    public function updateReport($request, $id)
    {
        $report = $this->getSingleReport('id', $id);
        if (!$report) return 'Report to be updated not found';
        $report->update($request->toArray());
        return $report;
    }


    /**
     * function to get find a report by its name
     *
     * @param $name
     * @return array
     */
    public function findReportByName($name)
    {
        $query = self::filterInput($name);

        $reports = $this->reportService->getFilteredReports('name', $name);

        if (is_string($reports)) return $reports;

        return $reports;
    }

    /**
     * @param $query
     * @return mixed
     */
    public function filterInput($query)
    {
        return str_replace('"', '', $query);
    }


    /**
     * @param $query
     * @return mixed|string
     */
    public function searchReportTable($query)
    {
        if (self::isSearchableFieldsSupplied($query)) {

            $key = array_key_first($query);

            if ($key == 'release_date') return self::searchTableByDate($key, $query[$key]);

            return self::searchTableByColumn($key, $query[$key]);
        }

        return 'invalid search key supplied';
    }


    /**
     * @param $table
     * @param $query
     * @return mixed
     */
    public function searchTableByColumn($table, $query)
    {
        return $this->report->where($table, 'LIKE', "%$query%")->get();
    }

    /**
     * @param $table
     * @param $query
     * @return mixed
     */
    public function searchTableByDate($table, $query)
    {
        return $this->report->whereDate($table, $query)->get();
    }

    /**
     * @param $data
     * @return int
     */
    public function isSearchableFieldsSupplied($data)
    {
        return count(array_intersect(array_keys($data), Report::$searchable_fields));
    }

}
