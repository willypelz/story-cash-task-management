<?php

/************************************
 ** File: Report Controller file  ******
 ** Date: 9th June 2020  ************
 ** Report Controller file  ************
 ** Author: Asefon pelumi M. *********
 ** Senior Software Developer ********
 * Email: pelumiasefon@gmail.com  ***
 * **********************************/

namespace App\Http\Controllers\Api\v1\Reports;

use App\Http\Controllers\Controller;
use App\Http\Library\RestFullResponse\ApiResponse;
use App\Http\Repository\ReportRepository;
use App\Http\Requests\CreateReportRequest;
use App\Http\Requests\UpdateReportRequest;
use App\Http\Resources\v1\Reports\ReportResource;
use App\Http\Resources\v1\Reports\ReportResourceCollection;
use App\Http\Resources\v1\Reports\ExternalReportResourceCollection;
use App\Http\Resources\v1\Reports\SingleReportResource;
use App\Models\Report;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ReportsController extends Controller
{

    protected $reportRepository;
    protected $apiResponse;
    protected $reportResource;


    /**
     * ReportsController constructor.
     * @param ReportRepository $reportRepository
     * @param ApiResponse $apiResponse
     * @param ReportResource $reportResource
     */
    public function __construct(
        ReportRepository $reportRepository,
        ApiResponse $apiResponse,
        ReportResource $reportResource
    )
    {
        $this->reportRepository = $reportRepository;
        $this->apiResponse = $apiResponse;
        $this->reportResource = $reportResource;
    }

    /**
     * @group Report management
     *
     *  Report Collection
     *
     * An Endpoint to get all Report in the system
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\Report\ReportResourceCollection
     * @apiResourceModel \App\Models\Report
     */
    public function index(Request $request)
    {

        if ($request->all()) {
            $reports = $this->reportRepository->searchReportTable($request->all());
            if (is_string($reports)) return $this->apiResponse->respondWithError($reports);
        } else {
            $reports = $this->reportRepository->getAllReports();
        }
        return $this->apiResponse->respondWithDataStatusAndCodeOnly(
            $this->reportResource->transformCollection($reports->toArray()), JsonResponse::HTTP_OK);
    }


    /**
     * @group Report management
     *
     *  Reports Collection
     *
     * An Endpoint to store report in the system
     *
     * @param CreateReportRequest $request
     * @param Report $report
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\Report\ReportResourceCollection
     * @apiResourceModel \App\Models\Report
     */
    public function store(CreateReportRequest $request, Report $report)
    {
        $report = $report->create($request->toArray());
        $report['hide_id'] = true;
        return $this->apiResponse->respondWithDataStatusAndCodeOnly(
            ['report' => $this->reportResource->transform($report)], JsonResponse::HTTP_CREATED);
    }


    /**
     * @group Report management
     *
     *  Reports Collection
     *
     * An Endpoint to get single Report detail in the system
     *
     * @param Report $report
     * @return \Illuminate\Http\JsonResponse
     * @apiResourceCollection \App\Http\Resources\v1\Report\ReportResourceCollection
     * @apiResourceModel \App\Models\Report
     */
    public function show(Report $report)
    {
        return $this->apiResponse->respondWithDataStatusAndCodeOnly(
            $this->reportResource->transform($report));
    }

    /**
     * An Endpoint to update the specified resource from storage.
     *
     * @param UpdateReportRequest $request
     * @return void
     */
    public function update(UpdateReportRequest $request, $id)
    {
        $updatedReport = $this->reportRepository->updateReport($request, $id);
        if (is_string($updatedReport)) return $this->apiResponse->respondWithError($updatedReport);

        return $this->apiResponse->respondWithNoPagination(
            $this->reportResource->transform($updatedReport),
            "The report $updatedReport->name was updated successfully");
    }


    /**
     * An Endpoint to Remove the specified resource from storage.
     *
     * @param Report $report
     * @return void
     * @throws \Exception
     */
    public function destroy(Report $report)
    {
        $report->delete();
        return $this->apiResponse->respondDeleted("The report $report->name was deleted successfully");
    }


    /**
     * An Endpoint to Get a report from external storage.
     *
     * @param Request $request
     * @return void
     */
    public function externalReport(Request $request)
    {
        $reportName = $request->name;
        //checking if the report was supplied
        if (empty($reportName)) return $this->apiResponse->respondWithError('Invalid Report name supplied');
        //finding the report
        $reportCollection = $this->reportRepository->findReportByName($reportName);
        //checking if it throws error

        if (is_string($reportCollection)) return $this->apiResponse->respondWithError('Error fetching the report from the api');
        //returning the final data
        return $this->apiResponse->respondWithDataStatusAndCodeOnly(new ExternalReportResourceCollection($reportCollection));
    }

}
