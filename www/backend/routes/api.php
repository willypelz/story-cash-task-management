<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*******************************************
 *******************************************
 *****  Reports API Version 1  ***************
 *******************************************
 *******************************************/


Route::namespace('Api\v1')->prefix('v1')->group(function () {
    Route::group(['namespace' => 'Reports'], function () {
        Route::resource('reports', 'ReportsController');
        Route::get('external-reports', 'ReportsController@externalReport')->name('reports.external');
    });
});
